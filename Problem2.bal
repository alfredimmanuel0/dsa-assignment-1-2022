import ballerina/graphql;
import ballerina/http;

listener http:Listener httpListener = check new(9000);


public type Covid19Stats record {|
    readonly string date;
    decimal totalDeaths;
    decimal confirmedCases;
    decimal recoveries;
    decimal testedCases;
    string region;
|};

table<Covid19Stats> key(date) covidInputStats = table [
    {date: "12/09/2021", region: "Khomas", confirmedCases: 465, totalDeaths: 39, recoveries: 67, testedCases: 1200}
    
];

public distinct service class CovidData {
    private final readonly & Covid19Stats entryRecord;

    function init(Covid19Stats entryRecord) {
        self.entryRecord = entryRecord.cloneReadOnly();
    }

    resource function get date() returns string {
        return self.entryRecord.date;
    }

    resource function get region() returns string {
        return self.entryRecord.region;
    }

    resource function get confirmedCases() returns decimal? {
        if self.entryRecord.confirmedCases is decimal {
            return self.entryRecord.confirmedCases / 1000;
        }
        return;
    }

    resource function get totalDeaths() returns decimal? {
        if self.entryRecord.totalDeaths is decimal {
            return self.entryRecord.totalDeaths / 1000;
        }
        return;
    }

    resource function get recoveries() returns decimal? {
        if self.entryRecord.recoveries is decimal {
            return self.entryRecord.recoveries / 1000;
        }
        return;
    }

    resource function get testedCases() returns decimal? {
        if self.entryRecord.testedCases is decimal {
            return self.entryRecord.testedCases / 1000;
        }
        return;
    }
}

service /covid19 on new graphql:Listener(9000) {
    resource function get all() returns CovidData[] {
        Covid19Stats[] covidEntries = covidInputStats.toArray().cloneReadOnly();
        return covidEntries.map(entry => new CovidData(entry));
    }

    resource function get filter(string data) returns CovidData? {
        Covid19Stats[] covid19Stats = covid19InputStats[date];
        if Covid19Stats is Covid19Stats {
            return new (Covid19Stats);
        }
        return;
    }

    remote function add(Covid19Stats entry) returns CovidData {
        covidInputStats.add(entry);
        return new CovidData(entry);
    }
    
}
