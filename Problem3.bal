import ballerina/http;



public type StudentEntry record {|
    readonly int studentNumber;
    string studentName;
    string course;
    string courseCode;
    string email;
    

    |}; 

    public final table<StudentEntry> key(studentNumber) studentTable = table [
        {studentNumber: 17, studentName: "Alfred Immanuel", course: "Computer Science", courseCode: "CS900", email: "alfredimmanuel0@gmail.com"}

    ];

    public type CreatedStudentEntries record {|
    *http:Created;
    StudentEntry[] body;
|};

public type ConflictingStudentNumbersError record {|
    *http:Conflict;
    ErrorMsg body;
|};

public type InvalidStudentNUmbersError record {|
    *http:NotFound;
    ErrorMsg body;
|};

public type ErrorMsg record {|
    string errmsg;
|};


service / on new http:Listener(9090) {

    resource function get student() returns   StudentEntry[] {
        return studentTable.toArray();
    }

    resource function post student(@http:Payload StudentEntry[] studentEntries)
                                    returns CreatedStudentEntries|ConflictingStudentNumbersError {

    int[] conflictingStudentNumbers = from StudentEntry StudentEntry in studentEntries
        where studentTable.hasKey(StudentEntry.studentNumber)
        select StudentEntry.studentNumber;

    if conflictingStudentNumbers.length() > 0 {
        return <ConflictingStudentNumbersError>{
            body: {
                errmsg: string:'join(" ", "Conflicting Student Numbers:",  ...conflictingStudentNumbers)
            }
        };
    } else {
        studentEntries.forEach(studentEntry => studentTable.add(studentEntry));
        return <CreatedStudentEntries>{body: studentEntries};
    }

}

    resource function get student/[string studentNumber]() returns StudentEntry|InvalidStudentNumberError {
    StudentEntry? StudentEntry = studentTable[studentNumber];
    if StudentEntry is () {
        return {
            body: {
                errmsg: string `Invalid Student Number: ${studentNumber}`
            }
        };
    }
    return StudentEntry;
}

}
